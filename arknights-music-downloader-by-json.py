import requests
import os
from json import loads

HEADERS = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0",
}
def main():
    f = open("arknights.json", "r")
    loaded_json = loads(f.read())
    f.close()
    for e in loaded_json:
        uri = e["url"]
        nameFile = e["name"] + ".mp3"
        if os.path.isfile(f"./{nameFile}"):
            continue
        r = requests.get(uri, headers=HEADERS)
        with open(nameFile, "wb") as f:
            f.write(r.content)
        print("Downloaded: " + nameFile)

if __name__ == "__main__":
    main()